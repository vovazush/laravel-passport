import store from '../store'

export default function (to, from, next) {
    if (store.getters.isUserLoggedIn || window.localStorage.getItem('token')) {
        next({path: '/'})
    } else {
        next()
    }
}