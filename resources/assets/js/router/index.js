import Vue from 'vue'
import Router from 'vue-router'
import AuthGuard from './auth-guard'
import GuestOnly from './guest-only'
import Home from '../components/Home'
import Main from '../components/Main'
import Login from '../components/Auth/Login'
import Logout from '../components/Auth/Logout'
import Register from '../components/Auth/Register'
import NotFound from '../components/NotFound'
import Users from '../components/Users/Users'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Main,
            children: [
                {
                    path: 'home',
                    alias: '',
                    component: Home,
                    beforeEnter: AuthGuard,
                    name: 'Home',
                },
                {
                    path: 'users',
                    alias: '',
                    component: Users,
                    beforeEnter: AuthGuard,
                    name: 'Users',
                },
            ]
        },
        {
            path: '/login',
            name: 'login',
            beforeEnter: GuestOnly,
            component: Login,
        },
        {
            path: '/logout',
            name: 'logout',
            beforeEnter: AuthGuard,
            component: Logout,
        },
        {
            path: '/register',
            name: 'register',
            beforeEnter: GuestOnly,
            component: Register,
        },
        {
            path: '*',
            component: NotFound
        }
    ],
    mode: 'history'
})
