import store from '../store'

export default function (to, from, next) {
    if (store.getters.isUserLoggedIn || window.localStorage.getItem('token')) {
        next()
    } else {
        next({
            path: '/login',
            query: { redirect: to.fullPath }
        })
    }
}