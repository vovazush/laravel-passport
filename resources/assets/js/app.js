import Vue from 'vue'
import Bootstrap from './bootstrap'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import router from './router'
import store from './store'
import App from './App.vue'

Vue.use(Bootstrap);

Vue.use(Vuetify, {
    theme: {
        primary: "#1E88E5",
        secondary: "#9575CD",
        accent: "#9c27b0",
        error: "#f44336",
        warning: "#3949AB",
        info: "#2196f3",
        success: "#4caf50"
    }
});

const app = new Vue({
    el: '#app',
    data: {
        appName: 'Service book'
    },
    store,
    router,
    components: { App },
    template: '<App :appName="appName"/>',
    created() {
        this.$store.dispatch('autoLoginUser')
    }
});
