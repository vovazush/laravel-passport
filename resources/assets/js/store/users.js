import axios from "axios/index";

class User {
    constructor(name, email, date, password = null, id = null) {
        this.name = name
        this.email = email
        this.date = date
        this.password = password
        this.id = id
    }
}

export default {
    state: {
        users: []
    },
    mutations: {
        loadUsers(state, payload) {
            state.users = payload
        }
    },
    actions: {
        async createUser({commit}, {email, name, password}) {
            commit('clearError')
            commit('setLoading', true)

            await axios.post('/api/users', {email, name, password})
                .then(({data}) => {
                    if (data.status === 201) {

                    } else {
                        commit('setError', data.message)
                        throw Error
                    }
                    commit('setLoading', false)
                })
                .catch(({response}) => {
                    commit('setLoading', false)
                    if (response.status === 401) {
                        this.$store.dispatch('logoutUser')
                    } else {
                        commit('setError', response.data.message)
                        throw new Error(JSON.stringify(response.data.errors))
                    }
                });
        },
        async fetchUsers({commit}) {
            commit('setLoading', true)
            commit('clearError')

            const resultUsers = []
            let users = [];

            try {
                await axios.get('/api/users')
                    .then(({data}) => {

                        if (data.status === 200) {
                            users = data.users
                        } else {
                            commit('setError', data.message)
                            throw Error
                        }
                        commit('setLoading', false)
                    })
                    .catch(({response}) => {
                        commit('setLoading', false)
                        if (response.status === 401) {
                            this.$store.dispatch('logoutUser')
                        } else {
                            commit('setError', response.data.message)
                            throw Error
                        }
                    });

                Object.keys(users).forEach(key => {
                    const u = users[key]
                    resultUsers.push(
                        new User(u.name, u.email, u.created_at, null, u.id)
                    )
                })

                commit('loadUsers', resultUsers)
                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
            }
        },
        async deleteUser({commit}, id) {
            commit('clearError')
            commit('setLoading', true)

            await axios.delete('/api/users/' + id)
                .then(({data}) => {
                    if (data.status === 201) {

                    } else {
                        commit('setError', data.message)
                        throw Error
                    }
                    commit('setLoading', false)
                })
                .catch(({response}) => {
                    commit('setLoading', false)
                    if (response.status === 401) {
                        this.$store.dispatch('logoutUser')
                    } else {
                        commit('setError', response.data.message)
                        throw new Error(JSON.stringify(response.data.errors))
                    }
                });
        },
        async updateUser({commit}, {id, email, name, password}) {
            commit('clearError')
            commit('setLoading', true)

            await axios.put('/api/users/' + id, {email, name, password})
                .then(({data}) => {
                    if (data.status === 201) {

                    } else {
                        commit('setError', data.message)
                        throw Error
                    }
                    commit('setLoading', false)
                })
                .catch(({response}) => {
                    commit('setLoading', false)
                    if (response.status === 401) {
                        this.$store.dispatch('logoutUser')
                    } else {
                        commit('setError', response.data.message)

                        throw new Error(JSON.stringify(response.data.errors))
                    }
                });
        },
    },
    getters: {
        users(state) {
            return state.users
        }
    }
}
