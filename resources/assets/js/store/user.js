import axios from "axios/index";

class User {
    constructor(token, user) {
        this.token = token;
        this.user = user;
    }
}

export default {
    state: {
        user: null
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload
        }
    },
    actions: {
        async loginUser({commit}, {email, password}) {
            commit('clearError')
            commit('setLoading', true)

            await axios.post('/api/login', {email, password})
                .then(({data}) => {
                    if (data.status === 200) {
                        window.localStorage.setItem('token', data.token);
                        window.localStorage.setItem('user', JSON.stringify(data.user));

                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;

                        commit('setUser', new User(data.user))
                    } else {
                        commit('setError', data.message)
                        throw Error
                    }
                    commit('setLoading', false)
                })
                .catch(({response}) => {
                    commit('setLoading', false)
                    if (response.status === 401) {
                        this.$store.dispatch('logoutUser')
                    } else {
                        commit('setError', response.data.message)
                        throw Error
                    }
                });
        },

        async registerUser({commit}, {email, name, password}) {
            commit('clearError')
            commit('setLoading', true)

            await axios.post('/api/register', {email, name, password})
                .then(({data}) => {
                    if (data.status === 201) {

                    } else {
                        commit('setError', data.message)
                        throw Error
                    }
                    commit('setLoading', false)
                })
                .catch(({response}) => {
                    commit('setLoading', false)
                    if (response.status === 401) {
                        this.$store.dispatch('logoutUser')
                    } else {
                        commit('setError', response.data.message)
                        throw Error
                    }
                });
        },

        autoLoginUser({commit}) {
            let token = window.localStorage.getItem('token');

            let userData = window.localStorage.getItem('user');
            let user = userData ? JSON.parse(userData) : null;

            if (token) {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                commit('setUser', new User(token, user))
            }
        },
        async logoutUser({commit}) {
            commit('clearError')
            commit('setLoading', true)

            await axios.post('/api/logout')
                .then(() => {
                    commit('setUser', null)

                    window.localStorage.removeItem('token');
                    window.localStorage.removeItem('user');
                    axios.defaults.headers.common['Authorization'] = '';
                    commit('setLoading', false)
                })
                .catch(({response}) => {
                    commit('setLoading', false)
                    commit('setError', response.data.message)
                });

        }
    },
    getters: {
        user(state) {
            return state.user
        },
        isUserLoggedIn(state) {
            return state.user !== null
        }
    }
}
