<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all(['id', 'name', 'email', 'created_at']);

        return response()->json([
            'users' => $users,
            'status' => 200
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users|max:191',
            'name' => 'required',
            'password' => 'required|min:6'
        ]);

        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

        return response()->json(['status' => 201]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'email' => [
                'required', 'email', 'max:191',
                Rule::unique('users')->ignore($user->id),
            ],
            'name' => 'required',
            'password' => 'nullable|min:6'
        ]);

        $validator->validate();

        $data = array_filter($data);

        if ($user->update($data)) {
            return response()->json([
                'status' => 201
            ]);
        }
        return response()->json([
            'status' => 422,
            'message' => 'Update user error'
        ]);
    }


    /**
     * Remove the specified resource from storage.
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $status = $user->delete();
        if ($status) {
            return response()->json([
                'status' => 201
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Delete user error'
            ]);
        }
    }
}
